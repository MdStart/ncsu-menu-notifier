## **Change Log - NCSU Menu Notifier**

### [1.4.2 | Opt out of summer notifications] (2017-05-19)
##### Added
- You can now specify whether or not you want notifications over the summer in your preferences.

---

### [1.4.1 | Keep you informed of updates] (2017-04-29)
##### Added
- This neat changelog!
- If there's an update to the site, we'll let you know at the end of one of your normal notifications so it's not too obtrusive.
- If it is still too obtrusive for you, you can disable this in your preferences.

##### Changed
- Made the verification codes all lowercase instead of uppercase for easier typing.
- Updated the info on the about page.

---

### [1.4.0 | Pick which meals you care about] (2017-04-22)
##### Added
- You can now specify which meals you want to be notified about in your preferences!
- If you click on your phone number on the preferences page, it will take you to an
API that you can use to change your preferences. Not terribly useful to you yet, but I think it's cool.

##### Changed
- Made changing preferences (specifically removing dishes) more user-friendly.

---

### [1.3.0 | Support for multiple dining halls] (2017-04-13)
##### Added
- The site now downloads data from Case and Clark in addition to Fountain!
- You can change which dining halls you care about on your preferences page.
- On mobile, you have the option of adding ncsu-mn to your home screen since it
is now a [progressive web app](https://developers.google.com/web/progressive-web-apps/).
- You can text "Demo" to see a preview text as if all your favorite foods were being served
at once.

##### Changed
- All pages now use [gzip compression](http://stackoverflow.com/a/16692852), so they load about 1.5-2x faster now.
- All menu items now use title case and have no leading or trailing whitespace, and
all duplicates have been removed.
- Overhauled the administrative side of the site.

---

### [1.2.0 | Web scraper rewrite] (2017-03-11)
##### Changed
- Re-wrote the part of ncsu-mn that downloads the menu so it works with the redesigned
dining.ncsu.edu.

---

### [1.1.0 | Software best practices] (2017-03-09)
##### Added
- The site now uses HTTPS to keep your data safe.
- Wrote software tests. About 90% of the lines of code have been tested.
- Added a code linter and a code style checker.
- Configured continuous integration to constantly test and lint the code.

##### Changed
- The login process is now completely asynchronous (no page refreshes required).
- When you try to login with invalid credentials, you'll get more descriptive error messages now.

---

### [1.0.0 | Initial release] (2016-10-12)
##### Added
- You can sign up with your phone number to receive notifications when they're serving
a specific dish at Fountain.
- You can log in to add more dishes to your account, or delete old ones.
- The website displays well on mobile and desktop.


[1.4.2 | Opt out of summer notifications]: https://gitlab.com/mac-chaffee/ncsu-menu-notifier/compare/v1.4.1...v1.4.2
[1.4.1 | Keep you informed of updates]: https://gitlab.com/mac-chaffee/ncsu-menu-notifier/compare/v1.4.0...v1.4.1
[1.4.0 | Pick which meals you care about]: https://gitlab.com/mac-chaffee/ncsu-menu-notifier/compare/v1.3.0...v1.4.0
[1.3.0 | Support for multiple dining halls]: https://gitlab.com/mac-chaffee/ncsu-menu-notifier/compare/v1.2.0...v1.3.0
[1.2.0 | Web scraper rewrite]: https://gitlab.com/mac-chaffee/ncsu-menu-notifier/compare/v1.1.0...v1.2.0
[1.1.0 | Software best practices]: https://gitlab.com/mac-chaffee/ncsu-menu-notifier/compare/v1.0.0...v1.1.0
[1.0.0 | Initial release]: https://gitlab.com/mac-chaffee/ncsu-menu-notifier/commits/v1.0.0
