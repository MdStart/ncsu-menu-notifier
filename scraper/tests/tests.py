import io
import json
import datetime
from decimal import Decimal, InvalidOperation
from unittest.mock import patch, call, PropertyMock
from django.test import TestCase
from django.core.management import call_command
from scraper.scraper_script import scrape, ALL_MEALS, ALL_HALLS, get_nutrition_facts
from scraper.management.commands.scrape import Command
from ncsumn.models import Meal, MenuItem, DiningHall


# Here are all the item_ids, which are also the names of the test files
ITEM_IDS = [
    "0_00000000_bad_html",
    "0_00000000_no_html",
    "0_00000000_bad_facts",
    "0_00000000_no_facts",
    "1_04052017_lunch_grill_stations_breaded_chicken_breast",
    "1_04052017_lunch_vegetable_carrot_coins",
    "1_04052017_lunch_vegetable_corn",
    "2_04052017_breakfast_dessert_chocolate_cinnamon_coffee_cake",
    "2_04052017_breakfast_entree_herb_baked_chicken",
    "2_04052017_breakfast_entree_quiche_lorraine",
    "2_04052017_breakfast_sauces_blueberry_puree",
    "2_04052017_dinner_display_crepe_station_with_fruittoppings",
    "2_04052017_lunch_entree_chili_con_carne",
    "2_04052017_lunch_entree_grilled_sesame_chicken",
    "2_04052017_lunch_entree_portobello_mushroom_stuffed_with_spinach",
    "3_04052017_breakfast_display_egg_whites",
    "3_04052017_breakfast_entree_herb_roasted_mushrooms",
    "3_04052017_breakfast_entree_shredded_hash_browns",

]
# Here are the items that are valid (cut off the first 2 diabolical tests)
VALID_ITEM_IDS = ITEM_IDS[2:]


class TestScraperScript(TestCase):
    """
    Given a specific html file downloaded 3/11/17, the scraper should
    always return data in a format that the rest of the app can recognize.
    """

    def setUp(self):
        """Remove the limit on maxDiff so error logs are easier to understand"""
        self.maxDiff = None

    @patch('scraper.scraper_script.urllib3.poolmanager.PoolManager.urlopen')
    def test_scrape_all_meals(self, mock_urlopen):
        # Make the urllib3's request.data return the contents of test files
        type(mock_urlopen.return_value).data = PropertyMock(side_effect=[load_test_file(m) for m in ALL_MEALS])
        # Run the scraper and check the results
        scrape_result = scrape('2017-03-11', 'Fountain')

        # It should have tried to request the correct url using GET
        base_url = ('https://dining.ncsu.edu/wp-admin/admin-ajax.php'
                    '?action=ncdining_ajax_menu_results&date=2017-03-11&pid=45&meal=')
        # Creates a list of tuples representing the expected arguments of each call
        expected_calls = [call('GET', base_url + meal_name) for meal_name in ALL_MEALS]
        # Make sure the correct calls were made
        mock_urlopen.assert_has_calls(expected_calls)

        # Make sure the correct data was returned
        self.assertEqual(get_expected_data('full_expected_data_031117'), scrape_result)

    @patch('scraper.scraper_script.urllib3.poolmanager.PoolManager.urlopen')
    def test_get_nutrition_facts(self, mock_urlopen):
        # Make the urllib3's request.data return the contents of test files
        type(mock_urlopen.return_value).data = PropertyMock(side_effect=[load_test_file(i) for i in ITEM_IDS])

        # Run get_nutrition_facts for every item_id
        for item in ITEM_IDS:
            actual_data = get_nutrition_facts(item)
            expected_data = get_expected_data('expected_%s' % item)
            self.assertEqual(expected_data, actual_data)


class TestScrapeCommand(TestCase):
    """
    Tests 'python manage.py scrape', which runs every day to update the menu in the DB
    """
    def setUp(self):
        # Make all meals
        for meal in ALL_MEALS:
            Meal.objects.create(name=meal)
        # Make all dining halls
        for hall in ALL_HALLS:
            DiningHall.objects.create(name=hall)
        # Make an existing MenuItem to test update_or_create. Must be a dish in test_menu_lunch.html
        dish = MenuItem.objects.create(name='Homemade Cheese, Onion & Pepper Frittata')
        dish.meals.add(Meal.objects.get(name='Lunch'))
        # Make a new MenuItem to test resetting is_being_served. Must NOT be in test_menu_lunch.html
        macaroni = MenuItem.objects.create(name='Macaroni', is_being_served=True)
        macaroni.meals.add(Meal.objects.get(name='Lunch'))

    @patch('scraper.management.commands.scrape.scrape')
    @patch('scraper.management.commands.scrape.MenuItem.has_nutrition_facts')
    def test_valid(self, mock_hnf, mock_scrape):
        # Make has_nutrition_facts always return true for this test
        mock_hnf.return_value = True
        # Make scrape() return valid data so we can test the command itself
        mock_scrape.return_value = get_expected_data('full_expected_data_031117')
        # Run the command. Should call scrape once per dining hall
        out = io.StringIO()
        call_command('scrape', stdout=out)
        expected_calls = [call(datetime.date.today(), hall) for hall in ALL_HALLS]
        mock_scrape.assert_has_calls(expected_calls, any_order=True)
        # Should have created a bunch of MenuItems
        self.assertEqual(61, MenuItem.objects.all().count())
        # Macaroni should have gotten is_being_served reset to False
        self.assertFalse(MenuItem.objects.get(name='Macaroni').is_being_served)
        # Homemade Cheese, Onion & Pepper Frittata is now being served
        self.assertTrue(
            MenuItem.objects.get(
                name='Homemade Cheese, Onion & Pepper Frittata'
            ).is_being_served
        )

    @patch('scraper.management.commands.scrape.scrape')
    @patch('scraper.management.commands.scrape.MenuItem.has_nutrition_facts')
    def test_no_data(self, mock_hnf, mock_scrape):
        # Make has_nutrition_facts always return true for this test
        mock_hnf.return_value = True
        # Make scrape() return empty data (pretend it's spring break)
        mock_scrape.return_value = {
            'Breakfast': [], 'Lunch': [],
            'Dinner': [], 'Brunch': [],
        }
        out = io.StringIO()
        call_command('scrape', stdout=out)
        expected_calls = [call(datetime.date.today(), hall) for hall in ALL_HALLS]
        mock_scrape.assert_has_calls(expected_calls, any_order=True)
        # Make sure no new MenuItems are created. Should have 2 from setUp.
        self.assertEqual(2, MenuItem.objects.all().count())

    @patch('scraper.management.commands.scrape.get_nutrition_facts')
    def test_create_menu_item(self, mock_gnf):
        """ Make sure DB entries are created based on scraped data """
        # Make get_nutrition_facts() return valid data
        valid_data = [get_expected_data('expected_' + i) for i in VALID_ITEM_IDS]
        mock_gnf.side_effect = valid_data
        # Create a fake item_dict which just contains eveything in VALID_ITEM_IDS
        fake_item_dicts = [{'name': i[11:], 'item_id': i} for i in VALID_ITEM_IDS]  # i[11:] cuts off the 1_12345678 part

        # Run create_menu_item on every item and check the results
        expected_items_in_db = 2
        for index, item in enumerate(fake_item_dicts):
            expected_items_in_db += 1
            Command().create_menu_item(item,
                                       Meal.objects.get(name='Lunch'),
                                       DiningHall.objects.get(name='Fountain'))
            mock_gnf.assert_called_with(item['item_id'])
            # Should have created a db entry with nutrition facts set
            self.assertEqual(expected_items_in_db, MenuItem.objects.all().count())
            created_item = MenuItem.objects.get(name=item['name'])
            for attr, label in MenuItem.NUTRITION_MAPPING.items():
                # Get what the attr is supposed to be and compare it to the actual value
                try:
                    expected_value = Decimal(valid_data[index][label])
                except InvalidOperation:
                    expected_value = Decimal('0.0')
                actual_value = getattr(created_item, attr)
                self.assertEqual(
                    expected_value, actual_value,
                    msg="%s didn't have its %s attribute set correctly!" % (created_item.name, attr)
                )


# Helper methods
def load_test_file(name):
    """
    Loads [name].html so we have consistent tests
    """
    with open('scraper/tests/test_files/%s.html' % name.lower(), 'rb') as test_data:
        return test_data.read()


def get_expected_data(file_name):
    """
    The scraper returns dictionaries which can be represented by json files.
    This function loads expected json from a file and converts it to a dict
    """
    with open('scraper/tests/test_files/%s.json' % file_name, 'r') as expected_data_file:
        return dict(json.load(expected_data_file))
