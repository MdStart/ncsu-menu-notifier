import os
import html
import datetime
import pprint
import urllib3
import certifi
from urllib3.exceptions import MaxRetryError
from bs4 import BeautifulSoup


ALL_MEALS = ['Breakfast', 'Lunch', 'Dinner', 'Brunch']
ALL_HALLS = ['Fountain', 'Clark', 'Case']


def get_data(meal, raw_html):
    """
    Turns raw HTML into a dictionary with a list of food items
    Format:
    {'Lunch':
        [
            {'name': 'Macaroni', 'item_id': '1_03132017_lunch_starches_macaroni'},
            {'name': 'Sandwich', 'item_id': '1_03132017_lunch_grains_sandwitch'},
            ...
        ]
    }
    """
    # Use beautifulsoup to get the data
    soup = BeautifulSoup(raw_html, "html.parser")
    # All food items are anchors with the 'dining-menu-item-modal' class
    raw_items = soup.find_all(class_='dining-menu-item-modal')
    if not raw_items:
        print('Found no items for %s' % meal)
        return {meal: []}

    print('Processing data for %s...' % meal)

    # Create a list of dictionaries where the keys are the name and the item_id
    dishes = []
    for dish in raw_items:
        # Use consistent formatting for the dish's name.
        # Remove html garbage and whitespace, then capitalize each word
        name = html.unescape(dish.text)
        name = name.strip()
        name = name.title()
        dishes.append({'name': name, 'item_id': dish.attrs['rel'][0]})
    # Put those dishes in a dictionary so we can associate them with the meal
    return {meal: dishes}


def scrape(date, location):
    """
    Makes a pseudo AJAX call to dining.ncsu.edu to download raw HTML
    for a given date at a given location.
    :param date: A Python date object.
    :param location: The name of a dining hall. Options: 'Fountain', 'Clark', 'Case'
    """
    print("Scraping %s's menu for %s..." % (location, str(date)))
    # Create a urllib3 instance that uses SSL
    http = urllib3.PoolManager(
        cert_reqs='CERT_REQUIRED',
        ca_certs=certifi.where()
    )
    # Construct the URL
    base_url = os.environ.get('SCRAPER_URL', 'https://dining.ncsu.edu/wp-admin/admin-ajax.php?action=ncdining_ajax_menu_results')
    # The date must be in this form: 2017-03-11, which is what str(date) returns
    date_param = '&date=%s' % str(date)
    # The locations depend on the WordPress page id.
    if location == 'Fountain':
        location_param = '&pid=45'
    elif location == 'Clark':
        location_param = '&pid=46'
    elif location == 'Case':
        location_param = '&pid=47'
    else:
        raise ValueError("The location must be in this list: %s" % ALL_HALLS)
    # Get the data for every meal by changing the meal_param
    all_meal_data = {}
    no_meal_url = base_url + date_param + location_param
    for meal in ALL_MEALS:
        full_url = no_meal_url + '&meal=%s' % meal
        try:
            # Make the request
            response = http.urlopen('GET', full_url)
            # Parse the data into a dictionary and add it to all_meal_data
            all_meal_data.update(get_data(meal, response.data))
        except MaxRetryError:
            # Couldn't connect. Keep looping in case it was just one meal that was broken
            continue
    return all_meal_data


def get_nutrition_facts(item_id):
    """
    Make a request for the nutrition facts of a given dish. The url is
    constructed based on the rel attribute of each dish. This method is called
    by the scrape command if it finds a dish without nutrition facts already saved.
    """
    print("Getting nutrition facts for %s" % item_id)
    base_url = "https://dining.ncsu.edu/wp-admin/admin-ajax.php?action=ncdining_ajax_get_item_nutrition&item_id="
    full_url = base_url + item_id
    http = urllib3.PoolManager(
        cert_reqs='CERT_REQUIRED',
        ca_certs=certifi.where()
    )
    # Make the request and have bs4 parse it
    response = http.urlopen('GET', full_url)
    soup = BeautifulSoup(response.data, "html.parser")

    # Start building the data
    nutrition_facts = {}
    # The nutrition facts are in rows with this class
    for row in soup.find_all(class_='menu-nutrition-row'):
        # The label is in the <strong> element with a colon at the end that must be removed
        label = html.unescape(row.find('strong').text).rstrip(':')
        # The value is in a span with the menu-nutrition-row-value class
        value = html.unescape(row.find(class_='menu-nutrition-row-value').text)
        nutrition_facts[label] = value

    # Get the allergens from a div
    allergen_div = soup.find(class_='dining-menu-allergen-contains')
    if allergen_div:
        nutrition_facts['Allergens'] = html.unescape(allergen_div.text)
    # Get the ingredients from another div
    ingredients_div = soup.find(class_='menu-dining-menu-modal-ingredients')
    if ingredients_div:
        nutrition_facts['Ingredients'] = html.unescape(ingredients_div.text).lstrip('Ingredients: ')

    return nutrition_facts
