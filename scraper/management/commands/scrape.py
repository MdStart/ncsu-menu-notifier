import datetime
from decimal import Decimal, InvalidOperation
from urllib3.exceptions import MaxRetryError
from django.core.management.base import BaseCommand
from scraper.scraper_script import scrape, get_nutrition_facts
from ncsumn.models import MenuItem, Meal, DiningHall


class Command(BaseCommand):
    help = 'Scrape the NCSU menu and save the results to the database'

    def handle(self, *args, **options):
        # Before doing anything, reset the meal, dining_hall, and is_being_served
        self.stdout.write(self.style.WARNING('Resetting all MenuItems to defaults for the new day...'))
        for item in MenuItem.objects.all():
            item.is_being_served = False
            for meal in item.meals.all():
                item.meals.remove(meal)
            for hall in item.locations.all():
                item.locations.remove(hall)
            item.save()

        # Make seperate calls to scraper_script for every dining hall (since it's a new url every time)
        for dining_hall in DiningHall.objects.all():
            data = scrape(datetime.date.today(), dining_hall.name)
            # Load all the menu items into the DB, or update them if they're already there
            for meal_name, item_list in data.items():
                # The meal for all these items will be the same
                meal = Meal.objects.get(name=meal_name)
                for item_dict in item_list:
                    self.create_menu_item(item_dict, meal, dining_hall)

    def create_menu_item(self, item_dict, meal, dining_hall):
        """
        Updates or creates MenuItems from the parameters. Also looks up
        nutrition facts if the db_entry doesn't have any
        Args:
            item_dict (dict): A dictionary from scrape() in this form:
                {'name': 'str', 'item_id': 'str'}
            meal (str): The name of the meal
            dining_hall (str): The name of the dining hall
        """
        # Either make a new entry for item or update the old item.is_being_served to True
        db_entry, was_created = MenuItem.objects.update_or_create(
            {'is_being_served': True},
            name=item_dict['name']
        )
        # Also update the meals and locations
        db_entry.meals.add(meal)
        db_entry.locations.add(dining_hall)
        # Get the nutrition facts if necessary
        if not db_entry.has_nutrition_facts:
            try:
                facts = get_nutrition_facts(item_dict['item_id'])

                if facts:
                    # Set the attributes of the MenuItem based on the mapping between attrs and labels
                    for attr, label in MenuItem.NUTRITION_MAPPING.items():
                        try:
                            setattr(db_entry, attr, Decimal(facts.get(label, '0.0')))
                        except InvalidOperation:
                            # Failed when trying to convert to decimal. Log it and keep going.
                            self.stdout.write(
                                self.style.ERROR('%s had a non-decimal value. Leaving the default value of 0.0...' % label)
                            )
                            continue
                    # Set the ingredients, allergens, and serving size seperate
                    db_entry.serving_size = facts.get('Serving Size', 'Unknown')
                    db_entry.allergens = facts.get('Allergens', 'Unknown')
                    db_entry.ingredients = facts.get('Ingredients', 'Unknown')
                else:
                    self.stdout.write(self.style.WARNING('Found no nutrition facts. Leaving them blank...'))

            except MaxRetryError:
                # Couldn't connect. Log it and keep going.
                self.stdout.write(
                    self.style.ERROR('Could not connect to dining.ncsu.edu for the nutrition facts. Leaving them blank...')
                )

        db_entry.save()
        if was_created:
            self.stdout.write(self.style.SUCCESS('Created entry for %s' % str(db_entry)))
        else:
            self.stdout.write(self.style.SUCCESS('Updated entry for %s' % str(db_entry)))
