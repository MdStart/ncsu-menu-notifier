# NCSU Menu Notifier

[![build status](https://gitlab.com/mac-chaffee/ncsu-menu-notifier/badges/master/build.svg)](https://gitlab.com/mac-chaffee/ncsu-menu-notifier/commits/master)
[![coverage report](https://gitlab.com/mac-chaffee/ncsu-menu-notifier/badges/master/coverage.svg)](https://gitlab.com/mac-chaffee/ncsu-menu-notifier/commits/master)



A website that will notify you when your favorite food is being served.

## How it works:
Uses a web scraper to download the menu every day. That data is
then parsed and saved in a database. Every day before every meal,
the site checks which dishes are being served for that meal and sends texts via
Twilio to the users who picked that dish.
