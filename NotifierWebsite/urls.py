from django.conf.urls import url, include
from django.conf import settings
from django.contrib import admin
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.views.generic.base import RedirectView
from NotifierWebsite.views import LetsEncrypt, ManifestJson

urlpatterns = [
    url(r'', include('ncsumn.urls')),

    url(regex=r'^admin/',
        view=admin.site.urls),

    url(regex=r'^%s$' % settings.SSL_URL,
        view=LetsEncrypt.as_view(),
        name='ssl'),

    url(regex=r'manifest.json/$',
        view=ManifestJson.as_view(),
        name='manifest'),

    url(regex=r'favicon.ico/$',
        view=RedirectView.as_view(url=static('favicon.png'), permanent=True),
        name='favicon'),
]
