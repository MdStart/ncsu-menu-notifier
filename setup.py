from os import path
from setuptools import setup, find_packages

HERE = path.abspath(path.dirname(__file__))
with open(path.join(HERE, 'README.md')) as f:
    LONG_DESCRIPTION = f.read()

setup(
    name='ncsu-menu-scraper',
    version='0.1',
    description='A web-scraper that reads the NCSU menu every day',
    long_description=LONG_DESCRIPTION,
    url='https://github.com/mac-chaffee/ncsu-menu-scraper/',
    author='Mac Chaffee and Xin Rao',
    license='MIT',
    keywords='web scraper ncsu menu north carolina state university dining fountain',
    packages=find_packages(),
    install_requires=['beautifulsoup'],
    entry_points={},
)
