import logging
from twilio.rest import Client
from ncsumn.models import SMSMessage
from django.utils import timezone
from django.conf import settings


def send_twilio_message(user, body):
    """
    Sends a text with the given body to the phone number
    in user.phone_number (only if DEBUG=False)
    """
    logger = logging.getLogger('Twilio')
    client = Client(settings.TWILIO_ACCOUNT_SID,
                    settings.TWILIO_AUTH_TOKEN)
    # Print the message if in debug mode
    if settings.DEBUG:
        logger.info('Fake Message to %s: %s', str(user), body)
    else:
        client.messages.create(
            body=body,
            to=user.phone_number,
            from_=settings.TWILIO_PHONE_NUMBER
        )
        logger.info('Sent a text to %s', str(user))

    # Always log the message
    SMSMessage.objects.create(time=timezone.now(),
                              direction="Sent",
                              user=user,
                              message=body)


def translate_list_to_english(lst):
    """
    Given a list of dicts, returns one long 'english' string.
    ['red', 'green', 'blue'] becomes 'red, green, and blue'
    :param lst: A list to translate
    :return: A plain-speech version of that list
    """
    final_string = ''
    if len(lst) == 2:
        final_string += lst[0]
        final_string += " and "
        final_string += lst[1]
    elif len(lst) == 1:
        final_string += lst[0]
    elif len(lst) == 0:
        final_string = ''
    else:
        for i in range(0, len(lst) - 1):
            final_string += lst[i]
            final_string += ", "
        final_string += "and "
        final_string += lst[len(lst) - 1]
    return final_string
