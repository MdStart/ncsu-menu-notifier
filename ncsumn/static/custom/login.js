// Post the phone number asynchronously to send a code
$("#login-form").submit(function(event) {
    event.preventDefault();
    // Create the loading icon
    $("#send-code").replaceWith("<div class='loading-pulse'></div>");
    $("#sent").replaceWith("<div class='loading-pulse'></div>");
    // POST the data
    $.ajax("/login/", {
      type: "POST",
      data: $(this).serialize(),
      success: function(data){
        // If there was no code, just display the data
        if ( $("#id_password").val().length === 0 )
          delay(showResults, data['username']);
        // Otherwise, check data. If it has errors, display them. Otherwise, go to prefs
        else if (data.hasOwnProperty('__all__')) {
          delay(showResults, data['__all__']);
        }
        else if (data.hasOwnProperty('username')) {
          delay(showResults, data['username']);
        }
        else {
          // If we got here, the entire form should be valid. Go to prefs.
          // Django won't let you see this page unless you're authed anyway.
          window.location.href = '/prefs/';
        }
      }
    });
});
function showResults(errors)
{
    $(".loading-pulse").replaceWith(`
      <div id='sent' class='centered'>
         <span class='text-muted'>${errors}</span>
       </div>
    `);
}
/**
 * Form validation is almost instant, but since it's asynchronous,
 * people won't know if it's actually doing anything. So delay all login attempts.
 */
function delay(otherFunction, errors)
{
  var start = new Date();
  var minDelay = 1300;
  var end = new Date();
  timePassed = end - start;
  if(timePassed > minDelay)
    otherFunction(errors);
  else
    setTimeout(function() { otherFunction(errors); }, minDelay - timePassed);
}

// Prevent the collapsible from closing
$("#send-code").click(function(event) {
    if($("#code-receiver").hasClass("in") || $("#id_username").val().length === 0) {
        event.stopPropagation();
    }
});
