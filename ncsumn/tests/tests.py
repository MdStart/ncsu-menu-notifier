import logging
from datetime import date
from unittest.mock import patch
from bs4 import BeautifulSoup
from django.test import TestCase, override_settings
from django.shortcuts import reverse
from django.contrib.auth.models import User
from django.contrib.auth import get_user
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.test import APITestCase
from ncsumn.models import NotifierUser, SMSMessage, MenuItem, Meal, DiningHall, Version
from ncsumn.forms import NOT_VERIFIED_ERROR, NO_PHONE_ERROR, INVALID_ERROR, BAD_CODE


def basic_setup():
    """
    Creates an auth User and a NotifierUser, as well as all meals and dining halls
    """
    user = User.objects.create(username='+19195152011')
    user.set_password('test-pass')  # Must use set_password so it hashes correctly
    user.save()
    NotifierUser.objects.create(phone_number='+19195152011', auth_user=user, verified=True)
    Meal.objects.create(name='Breakfast')
    Meal.objects.create(name='Brunch')
    Meal.objects.create(name='Lunch')
    Meal.objects.create(name='Dinner')
    DiningHall.objects.create(name="Fountain")
    DiningHall.objects.create(name="Clark")
    DiningHall.objects.create(name="Case")
    Version.objects.all().delete()  # Temporary fix for migrations creating inital versions
    Version.objects.create(version='v1.0.0', release_date=date(2016, 10, 12))


class TestGenericViews(TestCase):
    def setUp(self):
        basic_setup()
        self.logger = logging.getLogger('test')

    def test_all_views(self):
        """
        Visit every URL and assert that the status code <= 302.
        Just a sanity check.
        """
        self.logger.info('Visiting all normal urls...')
        all_url_names = ['home', 'confirm', 'prefs', 'login', 'about', 'changelog',
                         'ssl', 'manifest', 'favicon']

        for urlname in all_url_names:
            address = reverse(urlname)
            # Visit that url as a logged in user
            self.client.login(username='+19195152011', password='test-pass')
            response = self.client.get(address)
            self.assertTrue(response.status_code <= 302,
                            msg="Could not visit %s, got a %s" % (address, response.status_code))

    def test_ssl_view(self):
        """
        Make sure settings.SSL_URL returns the string settings.SSL_KEY
        """
        response = self.client.get('/' + settings.SSL_URL)
        self.assertEqual(response.content.decode('utf8'), settings.SSL_KEY)


class TestHome(TestCase):
    def setUp(self):
        basic_setup()

    def test_get_home(self):
        """Home view should send you to prefs if you're already logged in"""
        # GET when not authenticated should show the home screen
        response = self.client.get(reverse('home'))
        self.assertEqual(200, response.status_code)

        # GET when authenticated should redirect to prefs
        self.client.login(username='+19195152011', password='test-pass')
        response = self.client.get(reverse('home'))
        self.assertEqual(302, response.status_code)
        self.assertEqual(reverse('prefs'), response.url)


@override_settings(TWILIO_ACCOUNT_SID=settings.TWILIO_TEST_ACCOUNT_SID,
                   TWILIO_AUTH_TOKEN=settings.TWILIO_TEST_AUTH_TOKEN,
                   TWILIO_PHONE_NUMBER=settings.TWILIO_TEST_PHONE_NUMBER)
class TestLogin(TestCase):
    """
    Users must log in by inputting their phone number, receiving a code
    via text, and entering that code all within the same view.
    """
    def setUp(self):
        basic_setup()
        # Also create an un-verified user
        user = User.objects.create(username='+19199199119')
        user.set_password('test-pass')  # Must use set_password so it hashes correctly
        user.save()
        NotifierUser.objects.create(phone_number='+19199199119', verified=False, auth_user=user)
        self.logger = logging.getLogger('test')

    def test_bad_login(self):
        """
        Make sure bad phone numbers throw validation errors and don't send texts.
        Since we're manually POSTing, don't include country codes
        """
        # POST valid phone numbers that don't exist in the system
        non_existant_numbers = ['9999999999', '1234567890']
        self._try_login_with_error(non_existant_numbers, NO_PHONE_ERROR)
        # POST a number that exists, but is un-verified
        unverified_numbers = ['9199199119']
        self._try_login_with_error(unverified_numbers, NOT_VERIFIED_ERROR)
        # POST phone numbers that fail the regex validator
        garbage_numbers = [
            '9',
            '99999999999999999999',
            'asd',
            'qwertyuiop',
            '💻💻💻💻💻💻💻💻💻💻',
            '+19195152011'  # Including the country code should fail the validator
        ]
        self._try_login_with_error(garbage_numbers, INVALID_ERROR)

        # Now try posting a valid number with an invalid password
        bad_pass_data = {'username': '9195152011', 'password': '******'}
        response = self.client.post(reverse('login'), data=bad_pass_data)
        self.assertContains(response, BAD_CODE)

    def test_good_login(self):
        """
        Good phone numbers should not have validation errors and should send texts
        """
        # Just post the username, make sure you get a text and you're not logged in
        initial_data = {'username': '9195152011'}
        with patch('ncsumn.forms.get_random_string', return_value='TEST12'):
            response = self.client.post(reverse('login'), data=initial_data)
            self.assertContains(response, "Sent")
            self.assertLogs(logging.getLogger('Twilio'), 'Sent a text to +19195152011')
            self.assertEqual(1, SMSMessage.objects.all().count())
            self.assertFalse(get_user(self.client).is_authenticated())

        # Now post the username and code. Should log in and send no texts
        all_data = {'username': '9195152011', 'password': 'TEST12'}
        response = self.client.post(reverse('login'), data=all_data)
        self.assertRedirects(response, expected_url=reverse('prefs'))
        self.assertTrue(get_user(self.client).is_authenticated())

    def _try_login_with_error(self, bad_numbers, error):
        """
        Helper method to try to log in with every number in bad_numbers,
        making sure error is raised every time
        """
        for number in bad_numbers:
            self.logger.info('Trying to log in with bad number %s', number)
            response = self.client.post(reverse('login'), {'username': number})
            self.assertContains(response, error)
        # Bad numbers should not send any texts. All texts are logged in the DB, so check there
        self.assertFalse(SMSMessage.objects.all().exists(),
                         msg='Bad phone numbers should not trigger any text messages')


@override_settings(TWILIO_ACCOUNT_SID=settings.TWILIO_TEST_ACCOUNT_SID,
                   TWILIO_AUTH_TOKEN=settings.TWILIO_TEST_AUTH_TOKEN,
                   TWILIO_PHONE_NUMBER=settings.TWILIO_TEST_PHONE_NUMBER)
class TestSignupProcess(TestCase):
    def setUp(self):
        basic_setup()
        dinner = Meal.objects.get(name="Dinner")
        macaroni = MenuItem.objects.create(name='Macaroni')
        macaroni.meals.add(dinner)
        macaroni.save()

    def test_valid_signup(self):
        """
        POSTing valid data should update_or_create a NotifierUser and a User and verify them
        """
        # Post a new phone number and an existing dish
        valid_data = {'phone_number': '9195152000', 'favorite_dish': MenuItem.objects.first().id}
        response = self.client.post(reverse('home'), data=valid_data)
        # Should redirect to confirm, send a text, and create the user
        self.assertEqual(302, response.status_code)
        self.assertEqual(reverse('confirm'), response.url)
        self.assertLogs(logging.getLogger('Twilio'), 'Sent a text to +19195152000')
        try:
            n_user = NotifierUser.objects.get(phone_number='+19195152000')
            auth_user = User.objects.get(username='+19195152000')
        except ObjectDoesNotExist:
            self.fail("Signup should have created a NotifierUser and a User")
        self.assertEqual(auth_user, n_user.auth_user)

        # Post that same, unverified number. Should send another text to re-verify.
        response = self.client.post(reverse('home'), data=valid_data)
        # Should redirect to confirm, send a text, and create the user
        self.assertEqual(302, response.status_code)
        self.assertEqual(reverse('confirm'), response.url)
        self.assertLogs(logging.getLogger('Twilio'), 'Sent a text to +19195152000')
        try:
            n_user = NotifierUser.objects.get(phone_number='+19195152000')
        except ObjectDoesNotExist:
            self.fail("Signing up twice shouldn't delete the NotifierUser")
        # The user should be added to all 3 dining halls and all 4 meals by default
        self.assertEqual(3, n_user.locations.all().count())
        self.assertEqual(4, n_user.meals.all().count())
        # The user should have 1 fav_food
        self.assertEqual(1, n_user.fav_foods.all().count())
        self.assertEqual(valid_data['favorite_dish'], n_user.fav_foods.first().id)

    def test_existing_signup(self):
        """
        Existing users should be redirected to the login screen
        """
        # Post a new phone number and an existing dish
        valid_data = {'phone_number': '9195152011', 'favorite_dish': MenuItem.objects.first().id}
        response = self.client.post(reverse('home'), data=valid_data)
        # Should just redirect to login
        self.assertEqual(302, response.status_code)
        self.assertEqual(reverse('login'), response.url)

    def test_invalid_signup(self):
        """Diabolically bad POST data should show error messages"""
        bad_data = {'phone_number': 'text', 'favorite_dish': 9999}
        response = self.client.post(reverse('home'), data=bad_data)
        expected_errors = {
            'favorite_dish':
                ['Select a valid choice. That choice is not one of the available choices.'],
            'phone_number':
                ["Phone number must be entered in the format: '99999999'."]
        }
        # Make sure the errors are on the form
        self.assertEqual(expected_errors, response.context['form'].errors)


class TestPrefs(TestCase):
    def setUp(self):
        # Make a NotifierUser and assign them a MenuItem and some Locations.
        basic_setup()
        self.breakfast = Meal.objects.get(name='Breakfast')
        self.lunch = Meal.objects.get(name='Lunch')
        self.dinner = Meal.objects.get(name='Dinner')
        fountain = DiningHall.objects.get(name='Fountain')
        macaroni = MenuItem.objects.create(name='Macaroni')
        macaroni.meals.add(self.lunch)
        macaroni.save()

        self.n_user = NotifierUser.objects.first()
        self.n_user.fav_foods.add(macaroni)
        self.n_user.locations.add(fountain)
        self.n_user.meals.add(self.breakfast, self.lunch)
        self.n_user.save()

        # Make an extra MenuItem so we can change prefs later
        burritos = MenuItem.objects.create(name='Burritos')
        burritos.meals.add(self.lunch)
        burritos.save()

    def test_add_dishes(self):
        # Try to add Burritos
        switch_data = {'add_dish': MenuItem.objects.get(name='Burritos').id}
        self.client.login(username='+19195152011', password='test-pass')
        self.client.post(reverse('prefs'), switch_data, follow=True)
        self.assertIn(MenuItem.objects.get(name='Burritos'),
                      self.n_user.fav_foods.all())

    def test_update_meals(self):
        # Try to add Dinner and remove Breakfast
        switch_data = {'meals': [self.lunch.id, self.dinner.id]}
        self.client.login(username='+19195152011', password='test-pass')
        self.client.post(reverse('prefs'), switch_data, follow=True)
        # Should now have lunch and dinner only
        self.assertIn(self.lunch, self.n_user.meals.all())
        self.assertIn(self.dinner, self.n_user.meals.all())
        self.assertNotIn(self.breakfast, self.n_user.meals.all())

    def test_update_locations(self):
        self.client.login(username='+19195152011', password='test-pass')
        # Make sure the 'Fountain' checkbox starts out as checked="checked". Use beautifulsoup to check.
        response = self.client.get(reverse('prefs'))
        soup = BeautifulSoup(response.content, "html.parser")
        # A "checked" element with this id should exist
        self.assertTrue(soup.find(id="id_locations_2", checked=True))

        # Try to remove Fountain (with all boxes unchecked, empty data is sent)
        remove_data = {'locations': []}
        self.client.post(reverse('prefs'), remove_data, follow=True)
        self.assertEqual(0, self.n_user.locations.all().count(),
                         msg="The user should have had their locations cleared out.")

        # Try to add Case and Clark
        case_and_clark = DiningHall.objects.all().exclude(name='Fountain').order_by('name')
        add_data = {'locations': [hall.id for hall in case_and_clark]}
        self.client.post(reverse('prefs'), add_data, follow=True)
        self.assertEqual(2, self.n_user.locations.all().count(),
                         msg="The user should have 2 new locations")

        # Can't compare quersets, so create custom lists to see if the user's locations changed
        expected_locations = [hall.name for hall in case_and_clark]
        actual_locations = [hall.name for hall in self.n_user.locations.all().order_by('name')]
        self.assertEqual(expected_locations, actual_locations,
                         msg="The user's locations should now be 'Case' and 'Clark'")

    def test_update_opt_out(self):
        """Users can opt out of getting notified about site updates"""
        self.client.login(username='+19195152011', password='test-pass')
        self.assertTrue(self.n_user.wants_updates)
        change_data = {'wants_updates': False}
        self.client.post(reverse('prefs'), change_data, follow=True)
        self.n_user.refresh_from_db()
        self.assertFalse(self.n_user.wants_updates)

    def test_update_summer(self):
        """Users can opt out of getting notified over the summer"""
        self.client.login(username='+19195152011', password='test-pass')
        self.assertFalse(self.n_user.cares_about_summer)
        change_data = {'cares_about_summer': True}
        self.client.post(reverse('prefs'), change_data, follow=True)
        self.n_user.refresh_from_db()
        self.assertTrue(self.n_user.cares_about_summer)


@override_settings(TWILIO_ACCOUNT_SID=settings.TWILIO_TEST_ACCOUNT_SID,
                   TWILIO_AUTH_TOKEN=settings.TWILIO_TEST_AUTH_TOKEN,
                   TWILIO_PHONE_NUMBER=settings.TWILIO_TEST_PHONE_NUMBER)
class TestSMSResponse(APITestCase):
    """
    This view is used to verify phone numbers by checking if a known
    phone number sent a "CONFIRM" message.
    """
    def setUp(self):
        # Make a user and notifier user that is NOT verified yet
        basic_setup()
        n_user = NotifierUser.objects.first()
        n_user.verified = False
        n_user.save()
        self.logger = logging.getLogger('test')

    def test_verify(self):
        """
        Posting any variation of "Confirm" should verify the user and text them back
        """
        valid_data = {'Body': 'cOnFiRm', 'From': '+19195152011'}
        response = self.client.post(reverse('sms'), valid_data)
        self.assertEqual(200, response.status_code)
        self.assertTrue(NotifierUser.objects.first().verified)
        self.assertContains(response, "ncsu-mn: You're in! We'll text you when "
                                      "your favorite foods are being served!\n"
                                      "Change your preferences at ncsu-mn.com")

    def test_demo_invalid(self):
        """
        When a user asks for a demo, it should 404 if they're unverified, or send
        a message explaining that they don't have any fav_foods if they aren't setup yet
        """
        # Asking for a demo when not verified should 404
        data = {'Body': 'DeMo', 'From': '+19195152011'}
        response = self.client.post(reverse('sms'), data)
        self.assertEqual(404, response.status_code)

        # Verify the user and try again
        n_user = NotifierUser.objects.first()
        n_user.verified = True
        n_user.save()

        # This user has no foods, so it should tell them as much
        data = {'Body': 'demO', 'From': '+19195152011'}
        response = self.client.post(reverse('sms'), data)
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "You need to have at least one favorite food and "
                                      "at least one location to get a demo.")

    def test_demo_valid(self):
        """When the user has locations and fav_foods and asks for a demo, send a sample text"""
        # Verify, add a favorite food and location, and try again
        fountain = DiningHall.objects.get(name='Fountain')
        pancakes = MenuItem.objects.create(name='Pancakes')
        pancakes.locations.add(fountain)
        pancakes.save()
        n_user = NotifierUser.objects.first()
        n_user.verified = True
        n_user.fav_foods.add(pancakes)
        n_user.locations.add(fountain)
        n_user.save()

        # Should have a similar message to message_fans
        data = {'Body': 'DEMO', 'From': '+19195152011'}
        response = self.client.post(reverse('sms'), data)
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "They're serving Pancakes at Fountain today!")

    def test_invalid_post(self):
        self.logger.info("Testing invalid SMS POSTs (should 404)")
        # Random numbers sending texts should 404.
        invalid_data = {'Body': 'CONFIRM', 'From': '+11111111111'}
        response = self.client.post(reverse('sms'), invalid_data)
        self.assertEqual(404, response.status_code)
        self.assertFalse(NotifierUser.objects.first().verified)

        # Random text messages should 404
        invalid_data = {'Body': 'yo what up', 'From': '+19195152011'}
        response = self.client.post(reverse('sms'), invalid_data)
        self.assertEqual(404, response.status_code)
        self.assertFalse(NotifierUser.objects.first().verified)
