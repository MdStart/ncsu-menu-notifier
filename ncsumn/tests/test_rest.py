import datetime
from django.shortcuts import reverse
from django.contrib.auth.models import User
from django.conf import settings
from rest_framework.test import APITestCase
from ncsumn.tests.tests import basic_setup
from ncsumn.models import MenuItem, NotifierUser, DiningHall


class TestNotifierUserAPI(APITestCase):
    def setUp(self):
        basic_setup()
        n_user = NotifierUser.objects.first()
        macaroni = MenuItem.objects.create(
            name='Macaroni',
            is_being_served=True
        )
        n_user.fav_foods.add(macaroni)
        n_user.locations.add(DiningHall.objects.get(name='Fountain'))
        # Add an extra food that can be moved around
        MenuItem.objects.create(name='Spaghetti')
        # Make another user to test permissions
        other_user = User.objects.create_user(username='+19999999999', password='other-pass')
        NotifierUser.objects.create(phone_number='+19999999999', auth_user=other_user, verified=True)

        # Add re-used variables
        self.url = reverse('notifierusers', kwargs={'phone_number': '+19195152011'})

    def test_patch_delete(self):
        """Users can PATCH a MenuItem to remove it from their fav_foods"""
        self.client.login(username='+19195152011', password='test-pass')

        valid_data = {"op": "remove", "id": MenuItem.objects.get(name='Macaroni').id}
        response = self.client.patch(self.url, valid_data, format='json')

        # Should return 200 and remove macaroni from the user (but not from the db)
        self.assertEqual(200, response.status_code)
        self.assertEqual(0, NotifierUser.objects.get(
            phone_number='+19195152011'
        ).fav_foods.all().count())
        self.assertTrue(MenuItem.objects.get(name='Macaroni'))

    def test_patch_add(self):
        """Users can PATCH a MenuItem to add it to their fav_foods"""
        self.client.login(username='+19195152011', password='test-pass')

        valid_data = {"op": "add", "id": MenuItem.objects.get(name='Spaghetti').id}
        response = self.client.patch(self.url, valid_data, format='json')

        # Should return 200 and add macaroni to the user
        self.assertEqual(200, response.status_code)
        self.assertEqual(2, NotifierUser.objects.get(
            phone_number='+19195152011'
        ).fav_foods.all().count())

    def test_patch_invalid(self):
        """Return useful error messages when making bad API calls"""
        self.client.login(username='+19195152011', password='test-pass')
        bad_data = {'op': 'remove', 'id': 'some string'}
        response = self.client.patch(self.url, bad_data, format='json')
        self.assertEqual({'error': 'The id must be an integer'}, response.data)

        bad_data = {'op': 'something weird', 'id': MenuItem.objects.get(name='Spaghetti').id}
        response = self.client.patch(self.url, bad_data, format='json')
        self.assertEqual({'error': 'The operation must be "add" or "remove"'}, response.data)

    def test_get(self):
        """People can view the serialized version of their NotifierUser object"""
        self.client.login(username='+19195152011', password='test-pass')
        response = self.client.get(self.url)
        expected_data = {
            'phone_number': '+19195152011',
            'fav_foods': [MenuItem.objects.get(name='Macaroni').id],
            'locations': [DiningHall.objects.get(name='Fountain').id],
            'verified': True,
            'meals': []
        }
        self.assertEqual(expected_data, response.data)

    def test_put(self):
        """You can PUT a full serialized NotifierUser to change it"""
        self.client.login(username='+19195152011', password='test-pass')
        # Mkae the user unverified
        valid_data = {
            'phone_number': '+19195152011',
            'fav_foods': [MenuItem.objects.get(name='Macaroni').id],
            'locations': [DiningHall.objects.get(name='Fountain').id],
            'verified': False
        }
        response = self.client.put(self.url, valid_data, format='json')
        self.assertEqual(200, response.status_code)
        self.assertFalse(NotifierUser.objects.get(phone_number='+19195152011').verified)

    def test_invalid_patch(self):
        """People can only access the API for their account"""
        # Log in as other_user
        self.client.login(username='+19999999999', password='other-pass')
        # Try to access the main user's API with all the methods. Should 404
        response = self.client.get(self.url)
        self.assertEqual(404, response.status_code)
        valid_data = {"op": "remove", "id": MenuItem.objects.get(name='Macaroni').id}
        response = self.client.patch(self.url, valid_data, format='json')
        self.assertEqual(404, response.status_code)
        response = self.client.patch(self.url, valid_data, format='json')
        self.assertEqual(404, response.status_code)


class TestTagRelease(APITestCase):
    def setUp(self):
        basic_setup()
        self.valid_data = {
            "object_kind": "tag_push",
            "before": "0000000000000000000000000000000000000000",
            "after": "82b3d5ae55f7080f1e6022629cdb57bfae7cccc7",
            "ref": "refs/tags/v54.91.135",
        }

    def test_valid_webhook(self):
        """Valid webhooks should create Versions from the POST data"""
        # User should start out up_to_date when they create accounts
        self.assertTrue(NotifierUser.objects.first().is_up_to_date)
        # pylint: disable=no-member
        self.client.credentials(HTTP_X_GITLAB_TOKEN=settings.GITLAB_WEBHOOK_TOKEN)
        response = self.client.post(reverse('tag_release'), self.valid_data, format='json')

        self.assertEqual(200, response.status_code)
        self.assertEqual({'release_date': datetime.date.today(), 'version': 'v54.91.135'}, response.data)
        # That user should not be up_to_date anymore
        self.assertFalse(NotifierUser.objects.first().is_up_to_date)

    def test_invalid_webhook(self):
        """Invalid auth tokens and invalid tag names should return errors"""
        # Try to post with a missing token and valid data
        # pylint: disable=no-member
        self.client.credentials()
        response = self.client.post(reverse('tag_release'), self.valid_data, format='json')
        self.assertEqual(401, response.status_code)

        # Try to post an invalid token with valid data
        self.client.credentials(HTTP_X_GITLAB_TOKEN='badtoken')
        response = self.client.post(reverse('tag_release'), self.valid_data, format='json')
        self.assertEqual(401, response.status_code)

        # Try to post with with a good token, but an invalid tag ref
        self.client.credentials(HTTP_X_GITLAB_TOKEN=settings.GITLAB_WEBHOOK_TOKEN)
        invalid_data = {
            "object_kind": "tag_push",
            "before": "0000000000000000000000000000000000000000",
            "after": "82b3d5ae55f7080f1e6022629cdb57bfae7cccc7",
            "ref": "badtagwhichshouldfail",
        }
        response = self.client.post(reverse('tag_release'), invalid_data, format='json')
        self.assertEqual(400, response.status_code)
