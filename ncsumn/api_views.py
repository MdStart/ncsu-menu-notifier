import re
import datetime
from django.http import Http404, HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from rest_framework.views import APIView
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.response import Response
from rest_framework import status
from twilio.twiml.messaging_response import MessagingResponse
from ncsumn.utils import translate_list_to_english
from ncsumn.models import NotifierUser, Version
from ncsumn.serializers import NotifierUserSerializer


class NotifierUserAPI(RetrieveUpdateAPIView):
    """Allows people to view and update their own NotifierUser object"""
    serializer_class = NotifierUserSerializer

    # pylint: disable=arguments-differ
    def get_object(self, phone_number, user):
        """Only let people get their own NotifierUser object"""
        try:
            return NotifierUser.objects.get(phone_number=phone_number, auth_user=user)
        except (TypeError, ObjectDoesNotExist):
            raise Http404

    def retrieve(self, request, phone_number=None):
        """Called after a GET request"""
        item = self.get_object(phone_number, request.user)
        serializer = self.serializer_class(item)
        return Response(serializer.data)

    def update(self, request, **kwargs):
        """Called after a PUT request"""
        phone_number = kwargs.pop('phone_number')
        item = self.get_object(phone_number, request.user)
        serializer = self.serializer_class(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def partial_update(self, request, **kwargs):
        """
        Called after a PATCH request which adds/removes a given dish by id.
        Based loosely on https://tools.ietf.org/html/rfc6902#page-13
        """
        phone_number = kwargs.pop('phone_number')
        item = self.get_object(phone_number, request.user)
        # Parse the data to find the operation and the val to change
        operation = request.data.get('op')
        try:
            food_to_change = int(request.data.get('id'))
        except (TypeError, ValueError):
            return Response({'error': 'The id must be an integer'}, status=status.HTTP_400_BAD_REQUEST)
        old_fav_foods = [food.id for food in item.fav_foods.all()]
        if operation == 'remove':
            data = {'fav_foods': [food_id for food_id in old_fav_foods if food_id != food_to_change]}
        elif operation == 'add':
            data = {'fav_foods': old_fav_foods + [food_to_change]}
        else:
            return Response({'error': 'The operation must be "add" or "remove"'}, status=status.HTTP_405_METHOD_NOT_ALLOWED)
        # Attempt the partial update using 'data'
        serializer = self.serializer_class(item, data=data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SMSResponse(APIView):

    def post(self, request):
        # Get the some values from the POST data
        body = request.data.get('Body', '')
        from_number = request.data.get('From', '')
        twiml = MessagingResponse()

        # Check if the user is verifying
        if body.upper() == "CONFIRM":
            try:
                user = NotifierUser.objects.get(phone_number=from_number,
                                                verified=False)
                user.verified = True
                user.save()
                twiml.message("ncsu-mn: You're in! We'll text you when "
                              "your favorite foods are being served!\n"
                              "Change your preferences at ncsu-mn.com")
            except ObjectDoesNotExist:
                raise Http404
        # Check if the user wants a demo
        elif body.upper() == "DEMO":
            try:
                user = NotifierUser.objects.get(phone_number=from_number,
                                                verified=True)
            except ObjectDoesNotExist:
                raise Http404

            # Basically just do what message_fans does, but for all the user's dishes and locations
            all_foods = [food.name for food in user.fav_foods.all()]
            all_locations = [loc.name for loc in user.locations.all()]

            if all_foods and all_locations:
                twiml.message(
                    "They're serving %s at %s today!" % (translate_list_to_english(all_foods),
                                                         translate_list_to_english(all_locations)))
            else:
                twiml.message("You need to have at least one favorite food and "
                              "at least one location to get a demo.")
        else:
            raise Http404
        return HttpResponse(twiml.to_xml(), content_type='text/xml')


class TagRelease(APIView):
    """
    Endpoint for the GitLab webhook which triggers when a new tag is pushed.
    This view creates a new Version and prepares to text everyone about it.
    """
    def post(self, request):
        # Check for the token in the header
        token = request.META.get('HTTP_X_GITLAB_TOKEN')
        if token != settings.GITLAB_WEBHOOK_TOKEN:
            return Response({"error": "Invalid X-Gitlab-Token"}, status=401)
        # Parse the tag name out of 'ref': refs/tags/v0.0.0
        ref = request.data.get('ref')
        try:
            version = re.search("refs/tags/(.+)", ref).groups()[0]
        except AttributeError:
            return Response({"error": "Could not parse the tag name"}, status=400)

        obj = Version.objects.create(
            version=version,
            release_date=datetime.date.today()
        )
        # Now no one should be up to date
        for user in NotifierUser.objects.all():
            user.is_up_to_date = False
            user.save()

        return Response({"version": obj.version, "release_date": obj.release_date}, status=200)
