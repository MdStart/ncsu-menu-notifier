# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-17 02:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ncsumn', '0006_notifieruser_verified'),
    ]

    operations = [
        migrations.CreateModel(
            name='Meal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=10)),
            ],
        ),
        migrations.AddField(
            model_name='menuitem',
            name='is_being_served',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='menuitem',
            name='meal',
            field=models.ManyToManyField(to='ncsumn.Meal'),
        ),
    ]
