from django.db import models
from django.core.validators import RegexValidator
from django.contrib.auth.models import User


class Meal(models.Model):
    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name


class DiningHall(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class MenuItem(models.Model):
    name = models.CharField(max_length=100, unique=True)
    # These fields get changed after every scrape
    meals = models.ManyToManyField(Meal)
    is_being_served = models.BooleanField(default=False)
    locations = models.ManyToManyField(DiningHall)
    # Nutrition facts (don't change after initially set)
    serving_size = models.CharField(max_length=50, blank=True)
    calories = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    protein = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    total_carbs = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    total_fat = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    sat_fat = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    trans_fat = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    fiber = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    sodium = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    allergens = models.TextField(blank=True)
    ingredients = models.TextField(blank=True)

    class Meta:
        ordering = ['name']
        indexes = [models.Index(fields=['name'])]

    # Dict to covert between HTML names and field names
    NUTRITION_MAPPING = {
        'calories': 'Calories',
        'protein': 'Protein',
        'total_carbs': 'Total Carbohydrate',
        'total_fat': 'Total Fat',
        'sat_fat': 'Saturated Fat',
        'trans_fat': 'Trans Fat',
        'fiber': 'Fiber',
        'sodium': 'Sodium',
    }

    def __str__(self):
        if self.is_being_served:
            return self.name + ' ✔️'
        return self.name

    @property
    def has_nutrition_facts(self):
        """
        Checks if ANY nutrition fact fields have data.
        If any of them have data, that means we've scraped for
        the nutrition facts before and they probably never change.
        """
        # Look at all class attributes by looking at the keys in NUTRITION_MAPPING
        for attr, _ in self.NUTRITION_MAPPING.items():
            if getattr(self, attr):
                return True
        return False

    @property
    def num_fans(self):
        """
        Counts how many users have favorited this food
        """
        return NotifierUser.objects.filter(fav_foods=self).count()


class Version(models.Model):
    version = models.CharField(max_length=50)
    release_date = models.DateField()
    message = models.TextField(max_length=200, default="NCSU MN has been updated!")

    def __str__(self):
        return self.version


class NotifierUser(models.Model):
    auth_user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+19999999999'.")

    phone_number = models.CharField(validators=[phone_regex], max_length=15, blank=False, unique=True)
    verified = models.BooleanField(default=False)
    fav_foods = models.ManyToManyField(MenuItem, blank=True)
    locations = models.ManyToManyField(DiningHall, blank=True)
    meals = models.ManyToManyField(Meal, blank=True)
    is_up_to_date = models.BooleanField(default=True)
    wants_updates = models.BooleanField(default=True)
    cares_about_summer = models.BooleanField(default=False)

    class Meta:
        ordering = ['phone_number']
        indexes = [models.Index(fields=['phone_number'])]

    def __str__(self):
        if self.verified:
            return self.phone_number + ' ✔️'
        return self.phone_number + ' ❌'


class SMSMessage(models.Model):
    time = models.DateTimeField()
    direction = models.CharField(max_length=10)
    user = models.ForeignKey(NotifierUser)
    message = models.CharField(max_length=250)

    class Meta:
        ordering = ['-time']

    def __str__(self):
        return "%s at %s to/from %s" % (self.direction, self.time.__str__(), self.user.__str__())
